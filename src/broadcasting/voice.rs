use crate::State;
use std::sync::Arc;
use tbot::{
    contexts,
    markup::markdown_v2,
    state::messages::MessageId,
    types::{input_file::Voice, parameters::Text},
    util::entities,
};

pub async fn handler(context: Arc<contexts::Voice>, state: Arc<State>) {
    let sender_id = context.chat.id;
    let room = state.get_room(sender_id).await;

    if let Some(room) = room {
        let semaphore = state.room_locks.semaphore(&room).await;
        let _lock = semaphore.acquire().await;

        let caption = markdown_v2(entities(&context.caption)).to_string();
        let mut voice = Voice::id(&context.voice.file_id.0);

        if !caption.is_empty() {
            voice = voice.caption(Text::markdown_v2(&caption));
        }

        let mut recipients = state.participants(&room).await;
        recipients.retain(|&id| id != sender_id);
        let reply_to = state.get_messages_to_reply_to(&*context).await;
        let mut sent_messages = Vec::with_capacity(recipients.len());

        for id in recipients {
            let mut voice = context.bot.send_voice(id, voice);

            if let Some(&message_id) = reply_to.get(&id) {
                voice = voice.reply_to_message_id(message_id);
            }

            let call_result = voice.call().await;

            match call_result {
                Ok(message) => {
                    sent_messages.push(MessageId::from_message(&message));
                }
                Err(err) => {
                    dbg!(err);
                }
            }
        }

        state
            .save_broadcasted_messages(
                MessageId::from_context(&*context),
                sent_messages,
            )
            .await;
    }
}

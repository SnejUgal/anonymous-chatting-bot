use crate::State;
use std::sync::Arc;
use tbot::{
    contexts::fields,
    markup::{inline_code, markdown_v2},
    state::messages::MessageId,
    types::parameters::Text,
    util::entities,
};

pub async fn handler(context: Arc<impl fields::Text>, state: Arc<State>) {
    let sender_id = context.chat().id;
    let room = state.get_room(sender_id).await;

    if let Some(room) = room {
        let semaphore = state.room_locks.semaphore(&room).await;
        let _lock = semaphore.acquire().await;

        let mut recipients = state.participants(&room).await;
        recipients.retain(|&id| id != sender_id);
        let reply_to = state.get_messages_to_reply_to(&*context).await;
        let mut sent_messages = Vec::with_capacity(recipients.len());

        let entities = entities(context.text());

        let message =
            markdown_v2((inline_code(["$"]), " ", entities)).to_string();

        for id in recipients {
            let mut message =
                context.bot().send_message(id, Text::markdown_v2(&message));

            if let Some(&message_id) = reply_to.get(&id) {
                message = message.reply_to_message_id(message_id);
            }

            let call_result = message.call().await;

            match call_result {
                Ok(message) => {
                    sent_messages.push(MessageId::from_message(&message));
                }
                Err(err) => {
                    dbg!(err);
                }
            }
        }

        state
            .save_broadcasted_messages(
                MessageId::from_context(&*context),
                sent_messages,
            )
            .await;
    }
}

pub async fn edit_handler(context: Arc<impl fields::Text>, state: Arc<State>) {
    if let Some(messages) = state
        .get_broadcasted_messages(MessageId::from_context(&*context))
        .await
    {
        let entities = entities(context.text());

        let message =
            markdown_v2((inline_code(["$"]), " ", entities)).to_string();

        for MessageId {
            chat_id,
            message_id,
        } in messages
        {
            let call_result = context
                .bot()
                .edit_message_text(
                    chat_id,
                    message_id,
                    Text::markdown_v2(&message),
                )
                .call()
                .await;

            if let Err(err) = call_result {
                dbg!(err);
            }
        }
    }
}

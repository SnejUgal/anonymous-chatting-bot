use crate::State;
use std::sync::Arc;
use tbot::{
    contexts::Dice,
    state::messages::MessageId,
    types::{dice, input_file::Sticker},
};

pub async fn handler(context: Arc<Dice>, state: Arc<State>) {
    let sender_id = context.chat.id;
    let room = state.get_room(sender_id).await;

    if let Some(room) = room {
        let semaphore = state.room_locks.semaphore(&room).await;
        let _lock = semaphore.acquire().await;

        let sticker_pack = match context.dice.kind {
            dice::Kind::Dice => &state.dice,
            dice::Kind::Darts => &state.darts,
            _ => return,
        };

        let sticker = &sticker_pack[context.dice.value as usize - 1];
        let sticker = Sticker::id(&sticker.0);

        let mut recipients = state.participants(&room).await;
        recipients.retain(|&id| id != sender_id);
        let reply_to = state.get_messages_to_reply_to(&*context).await;
        let mut sent_messages = Vec::with_capacity(recipients.len());

        for id in recipients {
            let mut sticker = context.bot.send_sticker(id, sticker);

            if let Some(&message_id) = reply_to.get(&id) {
                sticker = sticker.reply_to_message_id(message_id);
            }

            let call_result = sticker.call().await;

            match call_result {
                Ok(message) => {
                    sent_messages.push(MessageId::from_message(&message));
                }
                Err(err) => {
                    dbg!(err);
                }
            }
        }

        state
            .save_broadcasted_messages(
                MessageId::from_context(&*context),
                sent_messages,
            )
            .await;
    }
}

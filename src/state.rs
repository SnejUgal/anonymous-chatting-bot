use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, sync::Arc};
use tbot::{
    contexts::fields,
    markup::{inline_code, markdown_v2},
    state::{
        messages::{MessageId, Messages},
        Chats,
    },
    types::{chat, file, message, parameters::Text},
    Bot,
};
use tokio::{
    fs::{read, write},
    join,
    sync::RwLock,
    task::block_in_place,
};

mod dice;
mod room_locks;
mod serde_rwlock;

use room_locks::RoomLocks;

const STATE_FILE: &str = "./state.ron";

lazy_static! {
    static ref PARTICIPANT_JOINED: String = markdown_v2((
        inline_code(["#"]),
        " A participant has joined the room.",
    ))
    .to_string();
    static ref PARTICIPANT_LEFT: String =
        markdown_v2((inline_code(["#"]), " A participant has left the room."))
            .to_string();
}

#[derive(Serialize, Deserialize, Default)]
pub struct State {
    #[serde(skip)]
    pub room_locks: RoomLocks,
    #[serde(skip)]
    pub dice: Vec<file::Id>,
    #[serde(skip)]
    pub darts: Vec<file::Id>,
    #[serde(with = "serde_rwlock")]
    chats: RwLock<Chats<String>>,
    #[serde(with = "serde_rwlock")]
    messages: RwLock<Messages<Vec<MessageId>>>,
}

impl State {
    pub async fn new(bot: &Bot) -> Self {
        let (mut state, dice, darts) = join!(
            State::parse_state(),
            dice::get_dice_sticker_pack(bot),
            dice::get_darts_sticker_pack(bot),
        );

        state.dice = dice;
        state.darts = darts;

        state
    }

    async fn parse_state() -> Self {
        let file = match read(STATE_FILE).await {
            Ok(file) => file,
            Err(..) => return State::default(),
        };

        ron::de::from_bytes(&file).unwrap()
    }

    async fn sync_to_file(&self) {
        let serialized = block_in_place(|| ron::ser::to_string(&self).unwrap());

        write(STATE_FILE, serialized).await.unwrap();
    }

    pub async fn participants(&self, room: &str) -> Vec<chat::Id> {
        self.chats
            .read()
            .await
            .iter()
            .filter_map(
                |(id, chat_room)| {
                    if chat_room == room {
                        Some(id)
                    } else {
                        None
                    }
                },
            )
            .collect::<Vec<_>>()
    }

    pub async fn join(&self, bot: &Bot, participant: chat::Id, room: String) {
        let previous_room = self
            .chats
            .write()
            .await
            .insert_by_id(participant, room.clone());

        if previous_room.as_deref() == Some(room.as_str()) {
            return;
        }

        self.notify(bot, &room, Text::markdown_v2(&PARTICIPANT_JOINED))
            .await;

        if let Some(room) = previous_room {
            self.notify(bot, &room, Text::markdown_v2(&PARTICIPANT_LEFT))
                .await;
        }

        self.sync_to_file().await;
    }

    pub async fn notify(&self, bot: &Bot, room: &str, message: Text<'_>) {
        let participants = self.participants(room).await;

        for id in participants {
            let call_result = bot.send_message(id, message).call().await;

            if let Err(err) = call_result {
                dbg!(err);
            }
        }
    }

    pub async fn leave(&self, bot: &Bot, participant: chat::Id) {
        let room = self.chats.write().await.remove_by_id(participant);

        if let Some(room) = room {
            self.notify(&bot, &room, Text::markdown_v2(&PARTICIPANT_LEFT))
                .await;
        }

        self.sync_to_file().await;
    }

    pub async fn get_room(&self, participant: chat::Id) -> Option<String> {
        self.chats
            .read()
            .await
            .get_by_id(participant)
            .map(|x| x.to_owned())
    }

    pub async fn save_broadcasted_messages(
        &self,
        original: MessageId,
        broadcasted: Vec<MessageId>,
    ) {
        self.messages
            .write()
            .await
            .insert_by_id(original, broadcasted);
        self.sync_to_file().await;
    }

    pub async fn get_broadcasted_messages(
        &self,
        original: MessageId,
    ) -> Option<Vec<MessageId>> {
        self.messages
            .read()
            .await
            .get_by_id(original)
            .map(|x| x.to_owned())
    }

    pub async fn sender_not_in_room(
        context: Arc<impl fields::Message>,
        state: Arc<Self>,
    ) -> bool {
        !state.chats.read().await.has(&*context)
    }

    pub async fn get_messages_to_reply_to(
        &self,
        context: &impl fields::MediaMessage,
    ) -> HashMap<chat::Id, message::Id> {
        let replied_to = match context.reply_to() {
            Some(reply_to) => MessageId {
                chat_id: reply_to.chat.id,
                message_id: reply_to.id,
            },
            None => return Default::default(),
        };

        self.messages
            .read()
            .await
            .iter()
            .find_map(|(original, broadcasted)| {
                if original == replied_to || broadcasted.contains(&replied_to) {
                    let map = broadcasted
                        .iter()
                        .map(|message| (message.chat_id, message.message_id))
                        .chain(Some((original.chat_id, original.message_id)))
                        .collect();
                    Some(map)
                } else {
                    None
                }
            })
            .unwrap_or_default()
    }
}

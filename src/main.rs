use dotenv::dotenv;
use lazy_static::lazy_static;
use rand::{distributions::Alphanumeric, prelude::*};
use std::sync::Arc;
use tbot::{
    contexts::fields::{Forward, Message},
    markup::{inline_code, markdown_v2},
    prelude::*,
    types::parameters::Text,
    Bot,
};

mod broadcasting;

mod state;
use state::State;

lazy_static! {
    static ref START_MESSAGE: String = markdown_v2((
        inline_code(["#"]),
        " Hello! I'm a bot for anonymous messaging in rooms. \
          Use the /create_room command to create a new room."
    ))
    .to_string();
    static ref HELP_MESSAGE: String = markdown_v2((
        inline_code(["#"]),
        " Here are the commands I know:\n\n\

          — /create_room — create rooms;\n\
          — /send — send messages (you can omit the command if you don't \
          need to send a command in the beginning of your message);\n\
          — /leave — leave the current room;\n\
          — /help — send this message.\n\n\

          To tell which messages are sent by the bot, its messages are \
          prefixed with a ",
        inline_code(["#"]),
        ", while other messages are prefixed with a ",
        inline_code(["$"]),
        ". Non-text messages are not prefixed, as they're always sent \
         by users.",
    ))
    .to_string();
    static ref YOU_JOINED: String =
        markdown_v2((inline_code(["#"]), " You have joined the room."))
            .to_string();
    static ref YOU_LEFT: String =
        markdown_v2((inline_code(["#"]), " You have left the room."))
            .to_string();
    static ref NOT_IN_ROOM: String = markdown_v2((
        inline_code(["#"]),
        " You have not joined a room to send messages. \
          Join one or create a room with /create_room."
    ))
    .to_string();
}

#[tokio::main]
async fn main() {
    let _ = dotenv();

    let bot = Bot::from_env("BOT_TOKEN");
    let me = bot.get_me().call().await.ok();
    let username = me
        .and_then(|me| me.user.username)
        .expect("Could not get username");

    let state = State::new(&bot).await;
    let mut bot = bot.stateful_event_loop(state);

    bot.start(|context, state| async move {
        if context.text.value.is_empty() {
            let call_result = context
                .send_message(Text::markdown_v2(&START_MESSAGE))
                .call()
                .await;

            if let Err(err) = call_result {
                dbg!(err);
            }
        } else {
            state
                .join(
                    &context.bot,
                    context.chat.id,
                    context.text.value.to_owned(),
                )
                .await;

            let call_result = context
                .send_message(Text::markdown_v2(&YOU_JOINED))
                .call()
                .await;

            if let Err(err) = call_result {
                dbg!(err);
            }
        }
    });

    bot.help(|context, _| async move {
        let call_result = context
            .send_message(Text::markdown_v2(&HELP_MESSAGE))
            .call()
            .await;

        if let Err(err) = call_result {
            dbg!(err);
        }
    });

    bot.animation_if(is_not_forwarded, broadcasting::animation::handler);
    bot.edited_animation(broadcasting::animation::edit_handler);

    bot.command_if("send", is_not_forwarded, broadcasting::text::handler);
    bot.text_if(is_not_forwarded, broadcasting::text::handler);

    bot.edited_command("send", broadcasting::text::edit_handler);
    bot.edited_text(broadcasting::text::edit_handler);

    bot.photo_if(is_not_forwarded, broadcasting::photo::handler);
    bot.edited_photo(broadcasting::photo::edit_handler);

    bot.document_if(is_not_forwarded, broadcasting::document::handler);
    bot.edited_document(broadcasting::document::edit_handler);

    bot.video_if(is_not_forwarded, broadcasting::video::handler);
    bot.edited_video(broadcasting::video::edit_handler);

    bot.sticker_if(is_not_forwarded, broadcasting::sticker::handler);

    bot.dice_if(is_not_forwarded, broadcasting::dice::handler);

    bot.video_note_if(is_not_forwarded, broadcasting::video_note::handler);

    bot.voice_if(is_not_forwarded, broadcasting::voice::handler);

    bot.animation_if(is_forwarded, forward);
    bot.audio_if(is_forwarded, forward);
    bot.command_if("send", is_forwarded, forward);
    bot.contact_if(is_forwarded, forward);
    bot.dice_if(is_forwarded, forward);
    bot.document_if(is_forwarded, forward);
    bot.game_if(is_forwarded, forward);
    bot.invoice_if(is_forwarded, forward);
    bot.location_if(is_forwarded, forward);
    bot.photo_if(is_forwarded, forward);
    bot.poll_if(is_forwarded, forward);
    bot.sticker_if(is_forwarded, forward);
    bot.text_if(is_forwarded, forward);
    bot.venue_if(is_forwarded, forward);
    bot.video_if(is_forwarded, forward);
    bot.video_note_if(is_forwarded, forward);
    bot.voice_if(is_forwarded, forward);

    bot.command_if("send", State::sender_not_in_room, warn_not_in_room);
    bot.text_if(State::sender_not_in_room, warn_not_in_room);
    bot.photo_if(State::sender_not_in_room, warn_not_in_room);
    bot.sticker_if(State::sender_not_in_room, warn_not_in_room);

    bot.command("leave", |context, state| async move {
        state.leave(&context.bot, context.chat.id).await;

        let call_result = context
            .send_message(Text::markdown_v2(&YOU_LEFT))
            .call()
            .await;

        if let Err(err) = call_result {
            dbg!(err);
        }
    });

    bot.command("create_room", move |context, state| {
        let username = username.clone();
        async move {
            let room = rand::thread_rng()
                .sample_iter(&Alphanumeric)
                .take(10)
                .collect::<String>();

            let message = markdown_v2((
                inline_code(["#"]),
                " You have created a room. Share this link so others can join \
                  your room: t.me/",
                username,
                "?start=",
                room.as_str(),
            ))
            .to_string();

            state.join(&context.bot, context.chat.id, room).await;

            let call_result = context
                .send_message(Text::markdown_v2(&message))
                .call()
                .await;

            if let Err(err) = call_result {
                dbg!(err);
            }
        }
    });

    if let Ok(url) = std::env::var("WEBHOOK_URL") {
        bot.webhook(&url, 5000).http().start().await.unwrap();
    } else {
        bot.polling().start().await.unwrap();
    }
}

async fn warn_not_in_room(context: Arc<impl Message>, _: Arc<State>) {
    let call_result = context
        .send_message(Text::markdown_v2(&NOT_IN_ROOM))
        .call()
        .await;

    if let Err(err) = call_result {
        dbg!(err);
    }
}

async fn is_forwarded(context: Arc<impl Forward>, _: Arc<State>) -> bool {
    context.forward().is_some()
}

async fn is_not_forwarded(context: Arc<impl Forward>, _: Arc<State>) -> bool {
    context.forward().is_none()
}

async fn forward(context: Arc<impl Forward>, state: Arc<State>) {
    let sender_id = context.chat().id;
    let room = state.get_room(sender_id).await;

    if let Some(room) = room {
        let semaphore = state.room_locks.semaphore(&room).await;
        let _lock = semaphore.acquire().await;

        let mut recipients = state.participants(&room).await;
        recipients.retain(|&id| id != sender_id);

        for id in recipients {
            let call_result = context
                .bot()
                .forward_message(id, context.chat().id, context.message_id())
                .call()
                .await;

            if let Err(err) = call_result {
                dbg!(err);
            }
        }
    }
}

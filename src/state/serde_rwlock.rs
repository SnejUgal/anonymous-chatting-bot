use serde::{Deserialize, Deserializer, Serialize, Serializer};
use tokio::{runtime::Handle, sync::RwLock};

pub fn deserialize<'de, D, T>(deserializer: D) -> Result<RwLock<T>, D::Error>
where
    D: Deserializer<'de>,
    T: Deserialize<'de>,
{
    Ok(RwLock::new(T::deserialize(deserializer)?))
}

pub fn serialize<S, T>(
    lock: &RwLock<T>,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: Serializer,
    T: Serialize,
{
    Handle::current()
        .block_on(async move { lock.read().await.serialize(serializer) })
}

pub use tbot::{types::file, Bot};

pub async fn get_dice_sticker_pack(bot: &Bot) -> Vec<file::Id> {
    bot.get_sticker_set("AnimatedDices")
        .call()
        .await
        .expect("Couldn't get dice sicker pack")
        .stickers
        .into_iter()
        .map(|x| x.file_id)
        .collect()
}

pub async fn get_darts_sticker_pack(bot: &Bot) -> Vec<file::Id> {
    bot.get_sticker_set("AnimatedDarts")
        .call()
        .await
        .expect("Couldn't get darts sicker pack")
        .stickers
        .into_iter()
        .skip(1)
        .map(|x| x.file_id)
        .collect()
}

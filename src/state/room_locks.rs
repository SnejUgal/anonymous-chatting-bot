use std::{collections::HashMap, sync::Arc};
use tokio::sync::{RwLock, Semaphore};

#[derive(Default)]
pub struct RoomLocks(RwLock<HashMap<String, Arc<Semaphore>>>);

impl RoomLocks {
    pub async fn semaphore(&self, room: &str) -> Arc<Semaphore> {
        if let Some(semaphore) = self.0.read().await.get(room) {
            return Arc::clone(&semaphore);
        }

        let semaphore = Arc::new(Semaphore::new(1));
        self.0
            .write()
            .await
            .insert(room.to_owned(), Arc::clone(&semaphore));

        semaphore
    }
}
